const { config } = require("dotenv");
const { Client } = require("pg");
const chargebee = require("chargebee");

config();

chargebee.configure({
  api_key: process.env.CHARGEBEE_API_KEY,
  site: process.env.CHARGEBEE_SITE,
});

const client = new Client({
  user: process.env.POSTGRES_USER,
  database: process.env.POSTGRES_DB,
  password: process.env.POSTGRES_PASSWORD,
  host: process.env.DJANGO_DATABASE_HOST,
  port: process.env.DJANGO_DATABASE_PORT,
});

async function fetch_accounts() {
  try {
    const res = await client.query(`
      select
        a.slug,
        a.name as company_name,
        u.first_name,
        u.last_name,
        u.email
      from
        account_account a
      join authentication_user u on
        u.id = a.owner_id
      where
        a.subscription_id = ''`);
    return res.rows;
  } catch (e) {
    console.log(err);
  }
}

async function find_subscription(customer_id) {
  console.log("find_subscription", customer_id);
  try {
    const response = await chargebee.subscription
      .list({
        limit: 1,
        "customer_id[is]": customer_id,
      })
      .request();

    return response.list[0];
  } catch (err) {
    console.log(err);
  }
}

async function create_subscription(account) {
  try {
    const response = await chargebee.subscription
      .create({
        plan_id: process.env.CHARGEBEE_TRIAL_PLAN_ID,
        plan_quantity: 50,
        customer: {
          id: account.slug,
          first_name: account.first_name,
          last_name: account.last_name,
          email: account.email,
          company: account.company_name,
        },
        billing_address: {
          first_name: account.first_name,
          last_name: account.last_name,
          email: account.email,
          company: account.company_name,
          validation_status: "valid",
        },
      })
      .request();

    return response;
  } catch (err) {
    if (err && err.api_error_code === "duplicate_entry") {
      console.log(
        `Customer '${account.slug}' already exist, fetching subscription`
      );
      return find_subscription(account.slug);
    }
    return;
  }
}

async function update_account(customer_id, subscription_id) {
  try {
    const query = `UPDATE account_account SET subscription_id = '${subscription_id}' WHERE slug = '${customer_id}'`;
    console.log(query);
    await client.query(query);
  } catch (err) {
    console.log(err);
  }
}

(async function main() {
  const skip_creation = process.argv.includes("--skip-creation");

  try {
    await client.connect();
    const accounts = await fetch_accounts();

    try {
      const subscriptions = skip_creation
        ? await Promise.all(
            accounts.map((acc) => acc.slug).map(find_subscription)
          )
        : await Promise.all(accounts.map(create_subscription));
      await Promise.all(
        subscriptions.map((data) => {
          if (!data) return;
          const { subscription } = data;

          return update_account(subscription.customer_id, subscription.id);
        })
      );
    } catch (err) {
      console.log(err);
    }
  } catch (err) {
    console.log(err);
  } finally {
    client.end();
  }
})();
